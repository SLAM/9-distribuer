package presles.models;

public class Etudiant extends Object
{
    private String nom;
    private String prenom;
    private String login;

    public Etudiant() {
        this.nom    = "anonymous";
        this.prenom = "";
        this.login  = "anonymous";
    }

    public Etudiant(String nom) {
        this.nom    =  nom;
        this.prenom =  "";
        this.login  = "anonymous";
    }

    public Etudiant(String nom, String prenom) {
        this.nom    =  nom;
        this.prenom =  prenom;
        this.login  = "";
    } 

    public Etudiant(String nom, String prenom, String login) {
        this.nom    =  nom;
        this.prenom =  prenom;
        this.login  = login;
    } 

    /**
    * rend le nom de l'étudiant
    *
    * @return le nom de l'étudiant 
    */
    public String getNom() {
        return this.nom;
    }  

    /**
    *  modifie le nom de l'étudiant
    *
    *  @param nom le nouveau nom de l'étudiant
    */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
    * rend le  prenom de l'étudiant
    *
    * @return le prenom de l'étudiant 
    */
    public String getPrenom() {
        return this.prenom;
    }  

    /**
    *  modifie le prenom de l'étudiant
    *
    *  @param prenom le nouveau prenom de l'étudiant
    */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    /**
    * rend le login  de l'étudiant
    *
    * @return le login de l'étudiant 
    */
    public String getLogin() {
        return this.login;
    }  

    /**
    *  modifie le login de l'étudiant
    *
    *  @param login le nouveau login de l'étudiant
    */
    public void setLogin(String login) {
        this.login = login;
    }

    public String toString() {
        return "Nom : " +this.nom +"\n"+
        "Prénom : " + this.prenom +"\n"+
        "Login : " + this.login;
    }

}
