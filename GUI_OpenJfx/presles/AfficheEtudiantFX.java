package presles;

import presles.models.Etudiant;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import java.util.List;


public class AfficheEtudiantFX extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Object e;
        Parameters params = getParameters();
        List<String> listArgs = params.getRaw();
        int nbArg = listArgs.size();
        if (nbArg == 1){ 
            e = new Etudiant(listArgs.get(0));
        }
        else if (nbArg == 2){
            e = new Etudiant(listArgs.get(0), listArgs.get(1));
        }
        else if (nbArg == 3){
            e = new Etudiant(listArgs.get(0), listArgs.get(1), listArgs.get(2));
        }
        else {
            e = new Etudiant();
        }
        // Object e = new Etudiant();
        Alert a = new Alert(AlertType.INFORMATION);
        a.setTitle("Information étudiant");
        a.setHeaderText("Profil de l'étudiant");
        a.setResizable(true);
        String content = String.format("%s", e);
        a.setContentText(content);
        a.showAndWait();
    }
}
