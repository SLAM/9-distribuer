package presles;

import presles.models.Etudiant;

public class AfficheEtudiant
{
    static public void main(String[] args) 
    {
        int nbarg =  args.length; 

        Etudiant e;

        if (nbarg == 1){ 
        e = new Etudiant(args[0]);
        }
        else if (nbarg == 2){
        e = new Etudiant(args[0], args[1]);
        }
        else if (nbarg == 3){
        e = new Etudiant(args[0], args[1], args[2]);
        }
        else
        e = new Etudiant();

        System.out.println(e.toString());
    }
}
