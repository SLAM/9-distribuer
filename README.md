# Utilisation de paquetages Java par la ligne de commande

Les paquetages standards de Java sont accessibles automatiquement par les commandes `javac` et `java`
(à condition bien sûr de mettre dans son programme les directives `import ...;` qui conviennent).

## Programme en ligne de commandes

On commence par se placer à la **racine** du projet
(c'est à dire, le répertoire dans lequel on place les fichiers du paquetage par défaut) :
```
cd CLI
```

### Compiler

Puis, on lance la compilation du programme principal :
```
javac presles/AfficheEtudiant.java
```
Cette dernière action génère les fichiers `presles/models/Etudiant.class` et
`presles/AfficheEtudiant.class`.

### Exécuter

On lance la commande :
```
java presles.AfficheEtudiant tux steph stux
```
Afin de préciser quelques arguments au programme...

### Distribuer

Dans le répertoire racine, on entre la commande :
```
jar cf AfficheEtudiant.jar presles/AfficheEtudiant.class presles/models/Etudiant.class
```
où, après `jar cf`, on met le nom du fichier à construire (ici `AfficheEtudiant.jar`),
puis tous les fichiers `.class` attendus.

> On peut utiliser le caractère `*`.

En fait, si on exécute le fichier `jar` (avec la commande `java -jar AfficheEtudiant.jar`),
on obtient une erreur, car nous n'avons pas déclaré le nom de la classe
principale dans un fichier *manifeste*.

On crée donc un fichier manifeste (que nous nommerons `AfficheEtudiant.mf`)
à la racine du projet, dont voici le contenu :
```
Manifest-Version: 1.0
Main-Class: presles.AfficheEtudiant
```

On relance la construction, en ajoutant une option supplémentaire à la commande `jar` :
```
jar cmf AfficheEtudiant.mf AfficheEtudiant.jar presles/AfficheEtudiant.class presles/models/Etudiant.class
```

Puis, on déplace le fichier `jar` dans un autre répertoire pour tester :
```
mv AfficheEtudiant.jar /tmp/
cd /tmp
java -jar AfficheEtudiant.jar tux steph stux
```

## Interface graphique avec OpenJfx

Ce second programme nécessite des librairies externes.

On commence par se placer à la **racine** du projet :
```
cd GUI_OpenJfx
```

### Compiler

Puis, on lance la compilation du programme principal :
```
javac --module-path /usr/share/openjfx/lib --add-modules=javafx.base,javafx.controls presles/AfficheEtudiantFX.java
```
Cette dernière action génère les fichiers `presles/models/Etudiant.class` et
`presles/AfficheEtudiantFX.class`.

### Exécuter

On lance la commande :
```
java --module-path /usr/share/openjfx/lib --add-modules=javafx.base,javafx.controls presles.AfficheEtudiantFX Tux Steph stux
```
Afin de préciser quelques arguments au programme...

## Itération Maven

Cette dernière itération du programme nécessite également des librairies externes,
mais la construction repose ici sur Maven.

On commence par se placer à la **racine** du projet :
```
cd Maven
```
Toute la configuration se trouve dans le fichier `pom.xml`.

### Exécuter

On lance la compilation du programme principal :
```
mvn clean javafx:run
```
Cette dernière action lance en amont la compilation (les fichiers sont générés dans
le répertoire `target`).

### Distribuer

Dans le répertoire racine, on entre la commande :
```
mvn package
```
> Le fichier manifeste `meta-inf/manifest.mf` est alimenté par les données
présentes dans le fichier `pom.xml`.

Puis, on déplace le fichier `jar` dans un autre répertoire pour tester :
```
mv target/etudiantfx-1.0-SNAPSHOT.jar /tmp/
cd /tmp
java -jar --module-path /usr/share/openjfx/lib --add-modules=javafx.controls etudiantfx-1.0-SNAPSHOT.jar Tux Steph stux
```
Avec quelques arguments supplémentaires pour le programme...
